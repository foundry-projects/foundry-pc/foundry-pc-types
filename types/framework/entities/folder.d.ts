declare class Folders extends EntityCollection<Folder> {}

declare class Folder extends Entity {
	/**
	 * Return an Array of the Entities which are contained within this Folder
	 */
	get entities(): Entity[];
}
