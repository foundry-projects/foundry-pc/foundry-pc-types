declare namespace Entity {
	/**
	 * Data structure common to all Entities.
	 * @template T The data template schema.
	 */
	interface Data<T = {}> {
		_id: string;
		name: string;
		folder: string;
		type: string;
		data: T;
		flags: Record<string, any>;
		permission: Record<string, number>;
		sort: number;
	}

	/**
	 * Creation options common to all Entities.
	 */
	interface Options {
		compendium: Compendium;
	}

	interface Config<E extends Entity = Entity> {
		baseEntity: E;
		collection: EntityCollection<E>;
		embeddedEntities: Record<string, string>;
		label: string;
		permissions: Record<string, string>;
	}
}

/**
 * An abstract class pattern for all primary data entities within the Foundry VTT Framework. An entity represents a
 * primary data concept, for example: Actor, Item, Scene, or ChatMessage. Each Entity type in Foundry Virtual
 * Tabletop extends this base Entity class which ensures similar behavior and workflow across all entity types.
 *
 * Documentation for this class is provided for reference, but developers should not extend this class directly,
 * instead work with or extend the Entity implementations that are referenced in this section of the API documentation.
 *
 * Entities are instantiated by providing their base data, and an optional Array of Application instances which should
 * be automatically refreshed when the Entity experiences an update.
 * @abstract
 * @interface
 *
 * @see {@link EntityCollection} The EntityCollection abstract class which contains Entity instances.
 * @see {@link Actor} The Actor Entity.
 * @see {@link Combat} The Combat Encounter Entity.
 * @see {@link Folder} The Folder Entity.
 * @see {@link Item} The Item Entity.
 * @see {@link JournalEntry} The Journal Entry Entity.
 * @see {@link ChatMessage} The Chat Message Entity.
 * @see {@link Playlist} The Audio Playlist Entity.
 * @see {@link Scene} The Scene Entity.
 * @see {@link RollTable} The Rollable Table Entity.
 * @see {@link User} The User Entity.
 * @see {@link Compendium} The Compendium which may contain Entities in a compendium pack.
 *
 * @param data					The data Object with which to create the Entity
 * @param options				Additional options which modify the created Entity behavior
 * @param options.compendium	A reference to the Compendium pack from which this Entity was drawn.
 *
 * @example
 * let actorData = {name: "John Doe", type: "character", img: "icons/svg/mystery-man.svg"};
 * let actor = new Actor(actorData);
 *
 * @template T	The template data interface that defines the properties stored in the database for this Entity
 */
declare class Entity<T = {}> {
	/**
	 * The original source data for the Entity provided upon initialization.
	 * This reflects the database state of the Entity before any transformations are applied.
	 */
	_data: Entity.Data<T>;

	/**
	 * The effective data for the Entity.
	 * This data object may have transformations applied to it.
	 */
	data: Entity.Data<T>;

	/**
	 * The options object that was used to configure the Entity upon initialization.
	 */
	options: Entity.Options;

	/**
	 * A collection of Application instances which should be re-rendered whenever this Entity experiences an update to
	 * its data. The keys of this object are the application ids and the values are Application instances. Each
	 * Application in this object will have its render method called by {@link Entity#render}.
	 */
	apps: Record<string, Application>;

	/**
	 * The Entity may optionally belong to a parent Compendium pack. If so this attribute will contain a reference
	 * to that Compendium object. Otherwise null.
	 */
	compendium: Compendium;

	constructor(data: Entity.Data<T>, options: Entity.Options);

	/**
	 * Safely Initialize data structure for the Entity.
	 * Errors that occur here should be captured and logged, but should not break construction of the Entity instance.
	 */
	_initialize(): void;

	/**
	 * Configure the attributes of this Entity class
	 * @param baseEntity		The parent class which directly inherits from the Entity interface.
	 * @param collection		The Collection instance to which Entities of this type belong.
	 * @param embeddedEntities	The names of any Embedded Entities within the Entity data structure.
	 */
	static get config(): Entity.Config;

	/**
	 * A Universally Unique Identifier (uuid) for this Entity instance
	 */
	get uuid(): string;

	/**
	 * Return a string which represents a dynamic link to this Entity.
	 */
	get link(): string;

	/**
	 * Prepare data for the Entity whenever the instance is first created or later updated.
	 * This method can be used to derive any internal attributes which are computed in a formulaic manner.
	 * For example, in a d20 system - computing an ability modifier based on the value of that ability score.
	 */
	prepareData(): any | void;

	/**
	 * Prepare Embedded Entities which exist within this parent Entity.
	 * For example, in the case of an Actor, this method is responsible for preparing the Owned Items the Actor contains.
	 */
	prepareEmbeddedEntities(): void;

	/**
	 * Obtain a reference to the Array of source data within the data object for a certain Embedded Entity name
	 * @param embeddedName	The name of the Embedded Entity type
	 * @return				The Array of source data where Embedded Entities of this type are stored
	 */
	getEmbeddedCollection(embeddedName: string): Array<any>;

	/**
	 * Render all of the Application instances which are connected to this Entity by calling their respective
	 * {@link Application#render} methods.
	 * @param force		Force rendering
	 * @param context	Optional context
	 */
	render(force: boolean, context?: any): void;

	/* -------------------------------------------- */
	/*  Properties
	/* -------------------------------------------- */

	/**
	 * Return a reference to the Collection instance which stores Entity instances of this type. This property is
	 * available as both a static and instance method and should be overridden by subclass Entity implementations.
	 */
	static get collection(): EntityCollection;

	/** @alias Entity.collection */
	get collection(): EntityCollection;

	/**
	 * The class name of the base Entity type, for example "Actor". This is useful in cases where there is an inheritance
	 * chain. Many places throughout the framework rely upon the canonical entity name which may not always be equal
	 * to the class name. This property is available as both a static and instance method.
	 *
	 * @example
	 * class Actor2ndGen extends Actor {...}
	 * Actor2ndGen.entity // "Actor"
	 */
	static get entity(): string;

	/** @alias Entity.entity */
	get entity(): string;

	/**
	 * A convenience accessor for the _id attribute of the Entity data object
	 */
	get id(): string;

	/** @alias Entity.id */
	get _id(): string;

	/**
	 * A convenience accessor for the name attribute of the Entity data object
	 */
	get name(): string;

	/**
	 * A property which gets or creates a singleton instance of the sheet class used to render and edit data for this
	 * particular entity type.
	 *
	 * @example <caption>A subclass of the Actor entity</caption>
	 * let actor = game.entities.actors[0];
	 * actor.sheet; // ActorSheet
	 */
	get sheet(): BaseEntitySheet;

	/**
	 * Obtain a reference to the BaseEntitySheet implementation which should be used to render the Entity instance
	 * configuration sheet.
	 */
	protected get _sheetClass(): any;

	/**
	 * Return a reference to the Folder which this Entity belongs to, if any.
	 *
	 * @example <caption>Entities may belong to Folders</caption>
	 * let folder = game.folders.entities[0];
	 * let actor = await Actor.create({name: "New Actor", folder: folder.id});
	 * console.log(actor.data.folder); // folder.id;
	 * console.log(actor.folder); // folder;
	 */
	get folder(): Folder | null;

	/**
	 * Return the permission level that the current game User has over this Entity.
	 * See the CONST.ENTITY_PERMISSIONS object for an enumeration of these levels.
	 *
	 * @example
	 * game.user.id; // "dkasjkkj23kjf"
	 * entity.data.permission; // {default: 1, "dkasjkkj23kjf": 2};
	 * entity.permission; // 2
	 */
	get permission(): number;

	/**
	 * A boolean indicator for whether or not the current game User has ownership rights for this Entity
	 * This property has a setter which allows for ownership rights to be overridden specifically on a per-instance basis
	 */
	get owner(): boolean;

	/**
	 * A boolean indicator for whether or not the current game User has at least limited visibility for this Entity.
	 */
	get visible(): boolean;

	/**
	 * A boolean indicator for whether the current game user has ONLY limited visibility for this Entity.
	 * Note that a GM user's perspective of an Entity is never limited.
	 */
	get limited(): boolean;

	/* -------------------------------------------- */
	/*  Permission Controls                         */
	/* -------------------------------------------- */

	/**
	 * Return an array of User entities who have a certain permission level or greater to the Entity.
	 * @param permission	The permission level or level name to test
	 * @param exact		Tests for an exact permission level match, by default this method tests for
	 *					an equal or greater permission level
	 * @returns			An array of User entities who match the permission level
	 */
	getUsers(permission: string | number, exact?: boolean): User[];

	/**
	 * Test whether a provided User a specific permission level (or greater) over the Entity instance
	 * @param user			The user to test for permission
	 * @param permission	The permission level or level name to test
	 * @param exact			Tests for an exact permission level match, by default this method tests for
	 *						an equal or greater permission level.
	 * @return				Whether or not the user has the permission for this Entity.
	 *
	 * @example <caption>Test whether a specific user has a certain permission</caption>
	 * // These two are equivalent
	 * entity.hasPerm(game.user, "OWNER");
	 * entity.owner;
	 * // These two are also equivalent
	 * entity.hasPerm(game.user, "LIMITED", true);
	 * entity.limited;
	 */
	hasPerm(user: User, permission: string | number, exact?: boolean): boolean;

	/**
	 * Test whether a given User has permission to perform some action on this Entity
	 * @param user		The User requesting creation
	 * @param action	The attempted action
	 * @param target	The targeted Entity
	 * @return			Does the User have permission?
	 */
	static can(user: User, action: string, target: Entity): boolean;

	/**
	 * Test whether a given User has permission to perform some action on this Entity
	 * @alias Entity.can
	 */
	can(user: User, action: string): boolean;

	/**
	 * Test for whether this Entity can be owned by any non-gamemaster player.
	 */
	get hasPlayerOwner(): boolean;

	/* -------------------------------------------- */
	/*  Entity Management Methods                   */
	/* -------------------------------------------- */

	/**
	 * Activate the Socket event listeners used to receive responses from events which modify database documents
	 * @param socket	The active game socket
	 */
	static activateSocketListeners(
		socket: SocketIOClient.Socket | WebSocket
	): void;

	/**
	 * Create one or multiple new entities using provided input data.
	 * Data may be provided as a single object to create one Entity, or as an Array of Objects.
	 * Entities may be temporary (unsaved to the database) by passing the temporary option as true.
	 * @static
	 *
	 * @param data					A Data object or array of Data
	 * @param options				Additional options which customize the creation workflow
	 * @param options.temporary		Create a temporary entity which is not saved to the world database. Default is false.
	 * @param options.renderSheet	Display the sheet for the created entity once it is created. Default is false.
	 * @param options.noHook		Block the dispatch of preCreate hooks for this operation.
	 *
	 * @return						The created Entity or array of Entities
	 *
	 * @example
	 * const data = {name: "New Entity", type: "character", img: "path/to/profile.jpg"};
	 * const created = await Entity.create(data); // Returns one Entity, saved to the database
	 * const temp = await Entity.create(data, {temporary: true}); // Not saved to the database
	 *
	 * @example
	 * const data = [{name: "Tim", type: "npc"], [{name: "Tom", type: "npc"}];
	 * const created = await Entity.create(data); // Returns an Array of Entities, saved to the database
	 * const created = await Entity.create(data, {temporary: true}); // Not saved to the database
	 */
	static create(data: object, options?: object): Promise<Entity>;

	/**
	 * Handle a SocketResponse from the server when one or multiple Entities are created
	 * @param {SocketRequest} request     The initial request
	 * @param {Data[]} result             An Array of created Entity data
	 * @param userId		The id of the requesting User
	 * @return				An Array of constructed Entity instances
	 * @private
	 */
	protected static _handleCreate({
		request,
		result,
		userId,
	}?: {
		request: any;
		result?: any[];
		userId: string;
	}): Entity[];

	/**
	 * Entity-specific actions that should occur when the Entity is first created
	 */
	protected _onCreate(data: object, options: object, userId: string): void;

	/**
	 * Update one or multiple existing entities using provided input data.
	 * Data may be provided as a single object to update one Entity, or as an Array of Objects.
	 * @static
	 *
	 * @param {Data|Data[]} data	A Data object or array of Data. Each element must contain the _id of an existing Entity.
	 * @param options			Additional options which customize the update workflow
	 * @param options.diff		Difference the provided data against the current to eliminate unnecessary changes.
	 * @param options.noHook	Block the dispatch of preUpdate hooks for this operation.
	 *
	 * @return					The updated Entity or array of Entities
	 *
	 * @example
	 * const data = {_id: "12ekjf43kj2312ds", name: "New Name"}};
	 * const updated = await Entity.update(data); // Updated entity saved to the database
	 *
	 * @example
	 * const data = [{_id: "12ekjf43kj2312ds", name: "New Name 1"}, {_id: "kj549dk48k34jk34", name: "New Name 2"}]};
	 * const updated = await Entity.update(data); // Returns an Array of Entities, updated in the database
	 */
	static update(data: object, options?: object): Entity;

	/**
	 * Handle a SocketResponse from the server when one or multiple Entities are updated
	 * @param {SocketRequest} request     The initial request
	 * @param {Data[]} result             An Array of updated Entity data
	 * @param {string} userId             The id of the requesting User
	 * @return {Entity[]}                 An Array of constructed Entity instances
	 */
	protected static _handleUpdate({
		request,
		result,
		userId,
	}?: {
		request: any;
		result?: any[];
		userId: string;
	}): Entity[];

	/**
	 * Entity-specific actions that should occur when the Entity is updated
	 */
	protected _onUpdate(data: object, options: object, userId: string): void;

	/**
	 * Update the current Entity using provided input data.
	 * Data must be provided as a single object which updates the Entity data.
	 * @see {Entity.update}
	 *
	 * @param {Data} data		A Data object which updates the Entity
	 * @param {Options} options	Additional options which customize the update workflow
	 * @return					The updated Entity
	 */
	update(data: object, options?: object): Promise<Entity>;

	/**
	 * Delete one or multiple existing entities using provided ids.
	 * The target ids may be a single string or an Array of strings.
	 *
	 * @param data				A single id or Array of ids
	 * @param options			Additional options which customize the deletion workflow
	 * @param options.noHook	Block the dispatch of preDelete hooks for this operation.
	 *
	 * @return					The deleted Entity or array of Entities
	 *
	 * @example
	 * const id = "12ekjf43kj2312ds";
	 * const deleted = await Entity.delete(id); // A single deleted entity from the database
	 *
	 * @example
	 * const ids = ["12ekjf43kj2312ds", "kj549dk48k34jk34"];
	 * const deleted = await Entity.delete(ids); // Returns an Array of deleted Entities
	 */
	static delete(
		data: string | string[],
		options?: object
	): Promise<Entity | Entity[]>;

	/**
	 * Handle a SocketResponse from the server when one or multiple Entities are deleted
	 * @param {SocketRequest} request     The initial request
	 * @param result	An Array of deleted Entity ids
	 * @param userId	The id of the requesting User
	 * @return			An Array of deleted Entity instances
	 */
	protected static _handleDelete({
		request,
		result,
		userId,
	}?: {
		request: any;
		result?: string[];
		userId: string;
	}): Entity[];

	/**
	 * Entity-specific actions that should occur when the Entity is deleted
	 */
	protected _onDelete(options: object, userId: string): void;

	/**
	 * Delete the current Entity.
	 * @see {Entity.delete}
	 *
	 * @param options	Options which customize the deletion workflow
	 * @return			The deleted Entity
	 */
	delete(options: object): Promise<Entity>;

	/* -------------------------------------------- */
	/*  Embedded Entity Management                  */
	/* -------------------------------------------- */

	/**
	 * Get an Embedded Entity by it's id from a named collection in the parent Entity.
	 *
	 * @param embeddedName	The name of the Embedded Entity type to retrieve
	 * @param id			The numeric ID of the child to retrieve
	 * @param strict		Throw an Error if the requested id does not exist, otherwise return null. Default false.
	 * @return				Retrieved data for the requested child, or null
	 */
	getEmbeddedEntity(
		embeddedName: string,
		id: string,
		{ strict }?: { strict?: boolean }
	): any;

	/**
	 * Create one or multiple EmbeddedEntities within this parent Entity.
	 * Data may be provided as a single Object to create one EmbeddedEntity or as an Array of Objects to create many.
	 * Entities may be temporary (unsaved to the database) by passing the temporary option as true.
	 *
	 * @param embeddedName			The name of the Embedded Entity class to create
	 * @param data					A Data object or an Array of Data objects to create
	 * @param options					Additional creation options which modify the request
	 * @param options.temporary		Create a temporary entity which is not saved to the world database. Default is false.
	 * @param options.renderSheet		Display the sheet for each created Embedded Entities once created.
	 * @param options.noHook			Block the dispatch of preUpdate hooks for this operation.
	 *
	 * @return						A Promise which resolves to the created embedded Data once the creation request is successful
	 *
	 * @example
	 * const actor = game.actors.get("dfv934kj23lk6h9k");
	 * const data = {name: "Magic Sword", type: "weapon", img: "path/to/icon.png"};
	 * const created = await actor.createEmbeddedEntity("OwnedItem", data); // Returns one EmbeddedEntity, saved to the Actor
	 * const temp = await actor.createEmbeddedEntity("OwnedItem", data, {temporary: true}); // Not saved to the Actor
	 *
	 * @example
	 * const actor = game.actors.get("dfv934kj23lk6h9k");
	 * const data = [{name: "Mace of Crushing", type: "weapon"}, {name: "Shield of Defense", type: "armor"}];
	 * const created = await actor.createEmbeddedEntity("OwnedItem", data); // Returns an Array of EmbeddedEntities, saved to the Actor
	 * const temp = await actor.createEmbeddedEntity("OwnedItem", data, {temporary: true}); // Not saved to the Actor
	 */
	createEmbeddedEntity(
		embeddedName: string,
		data: object | object[],
		options?: object
	): Promise<Entity>;

	/**
	 * Handle a SocketResponse from the server when one or multiple Embedded Entities are created
	 * @param {SocketRequest} request     The initial request
	 * @param {Data[]} result             An Array of created Entity data
	 * @param {string} userId             The id of the requesting User
	 * @return {Data[]}                   An Array of constructed EmbeddedDocument data
	 * @private
	 */
	protected static _handleCreateEmbeddedEntity({
		request,
		result,
		userId,
	}: {
		request: any;
		result?: any[];
		userId: string;
	}): any[];

	/**
	 * Handle Embedded Entity creation within this Entity with specific callback steps.
	 * This function is triggered once per EmbeddedEntity which is updated.
	 * It therefore may run multiple times per creation workflow.
	 * Any steps defined here should run on a per-EmbeddedEntity basis.
	 * Steps that should run once for the whole batch should go in _onModifyEmbeddedEntity()
	 */
	protected _onCreateEmbeddedEntity(
		embeddedName: string,
		child: any,
		options: object,
		userId: string
	): void;

	/**
	 * Update one or multiple existing entities using provided input data.
	 * Data may be provided as a single object to update one Entity, or as an Array of Objects.
	 *
	 * @param embeddedName		The name of the Embedded Entity class to create
	 * @param data				A Data object or array of Data. Each element must contain the _id of an existing Entity.
	 * @param options			Additional options which customize the update workflow
	 * @param options.diff		Difference the provided data against the current to eliminate unnecessary changes.
	 * @param options.noHook	Block the dispatch of preUpdate hooks for this operation.
	 *
	 * @return					The updated Entity or array of Entities
	 *
	 * @example
	 * const actor = game.actors.get("dfv934kj23lk6h9k");
	 * const item = actor.data.items.find(i => i.name === "Magic Sword");
	 * const update = {_id: item._id, name: "Magic Sword +1"};
	 * const updated = await actor.updateEmbeddedEntity("OwnedItem", update); // Updates one EmbeddedEntity
	 *
	 * @example
	 * const actor = game.actors.get("dfv934kj23lk6h9k");
	 * const weapons = actor.data.items.filter(i => i.type === "weapon");
	 * const updates = weapons.map(i => {
	 *   return {_id: i._id, name: i.name + "+1"};
	 * }
	 * const updated = await actor.createEmbeddedEntity("OwnedItem", updates); // Updates multiple EmbeddedEntity objects
	 */
	updateEmbeddedEntity(
		embeddedName: string,
		data: object | object[],
		options?: object
	): Promise<Entity | Entity[]>;

	/**
	 * Handle a SocketResponse from the server when one or multiple Embedded Entities are updated
	 * @param {SocketRequest} request     The initial request
	 * @param {Data[]} result             An Array of updated Entity data
	 * @param {string} userId             The id of the requesting User
	 * @return {Data[]}                   An Array of updated EmbeddedDocument data
	 */
	protected static _handleUpdateEmbeddedEntity({
		request,
		result,
		userId,
	}: {
		request: any;
		result?: any[];
		userId: string;
	}): any[];

	/**
	 * Handle Embedded Entity updates within this Entity with specific callback steps.
	 * This function is triggered once per EmbeddedEntity which is updated.
	 * It therefore may run multiple times per creation workflow.
	 * Any steps defined here should run on a per-EmbeddedEntity basis.
	 * Steps that should run once for the whole batch should go in _onModifyEmbeddedEntity()
	 */
	protected _onUpdateEmbeddedEntity(
		embeddedName: string,
		child: any,
		options: object,
		userId: string
	): void;

	/**
	 * Delete one or multiple existing EmbeddedEntity objects using provided input data.
	 * Data may be provided as a single id to delete one object or as an Array of string ids.
	 *
	 * @param embeddedName		The name of the Embedded Entity class to create
	 * @param data				A Data object or array of Data. Each element must contain the _id of an existing Entity.
	 * @param options			Additional options which customize the update workflow
	 * @param options.noHook	Block the dispatch of preUpdate hooks for this operation.
	 *
	 * @return					The deleted Embedded Entities
	 *
	 * @example
	 * const actor = game.actors.get("dfv934kj23lk6h9k");
	 * const item = actor.data.items.find(i => i.name === "Magic Sword");
	 * const deleted = await actor.deleteEmbeddedEntity("OwnedItem", item._id); // Deletes one EmbeddedEntity
	 *
	 * @example
	 * const actor = game.actors.get("dfv934kj23lk6h9k");
	 * const weapons = actor.data.items.filter(i => i.type === "weapon");
	 * const deletions = weapons.map(i => i._id);
	 * const deleted = await actor.deleteEmbeddedEntity("OwnedItem", deletions); // Deletes multiple EmbeddedEntity objects

	 */
	deleteEmbeddedEntity(
		embeddedName: string,
		data: object | object[],
		options?: object
	): Promise<any | any[]>;

	/**
	 * Handle a SocketResponse from the server when one or multiple Embedded Entities are deleted
	 * @param {SocketRequest} request     Th2e initial request
	 * @param {string[]} result           An Array of deleted EmbeddedEntity ids
	 * @param {string} userId             The id of the requesting User
	 * @return {Data[]}                   An Array of deleted EmbeddedDocument data
	 */
	protected static _handleDeleteEmbeddedEntity({
		request,
		result,
		userId,
	}: {
		request: any;
		result?: any[];
		userId: string;
	}): any[];

	/**
	 * Handle Embedded Entity deletion within this Entity with specific callback steps.
	 * This function is triggered once per EmbeddedEntity which is updated.
	 * It therefore may run multiple times per creation workflow.
	 * Any steps defined here should run on a per-EmbeddedEntity basis.
	 * Steps that should run once for the whole batch should go in _onModifyEmbeddedEntity()
	 */
	protected _onDeleteEmbeddedEntity(response: any): void;

	/**
	 * A generic helper since we take the same actions for every type of Embedded Entity update
	 * Unlike the specific _onCreate, _onUpdate, and _onDelete methods this only runs once per updated batch
	 */
	protected _onModifyEmbeddedEntity(
		embeddedName: string,
		changes: any,
		options: any,
		userId: any,
		context: any
	): void;

	/* -------------------------------------------- */
	/*  Data Flags                                  */
	/* -------------------------------------------- */

	/**
	 * Get the value of a "flag" for this Entity
	 * See the setFlag method for more details on flags
	 *
	 * @param scope	The flag scope which namespaces the key
	 * @param key	The flag key
	 * @return		The flag value
	 */
	getFlag<F = any>(scope: string, key: string): F;

	/**
	 * Assign a "flag" to this Entity.
	 * Flags represent key-value type data which can be used to store flexible or arbitrary data required by either
	 * the core software, game systems, or user-created modules.
	 *
	 * Each flag should be set using a scope which provides a namespace for the flag to help prevent collisions.
	 *
	 * Flags set by the core software use the "core" scope.
	 * Flags set by game systems or modules should use the canonical name attribute for the module
	 * Flags set by an individual world should "world" as the scope.
	 *
	 * Flag values can assume almost any data type. Setting a flag value to null will delete that flag.
	 *
	 * @param scope	The flag scope which namespaces the key
	 * @param key	The flag key
	 * @param value	The flag value
	 *
	 * @return		A Promise resolving to the updated Entity
	 */
	setFlag<F = any>(scope: string, key: string, value: F): Promise<Entity<T>>;

	/**
	 * Remove a flag assigned to the Entity
	 * @param scope	The flag scope which namespaces the key
	 * @param key	The flag key
	 * @return		A Promise resolving to the updated Entity
	 */
	unsetFlag(scope: string, key: string): Promise<Entity>;

	/* -------------------------------------------- */
	/*  Sorting                                     */
	/* -------------------------------------------- */

	/**
	 * Sort this Entity relative a target by providing the target, an Array of siblings and other options.
	 * See SortingHelper.performIntegerSort for more details
	 */
	sortRelative({
		target,
		siblings,
		sortKey,
		sortBefore,
		updateData,
	}: {
		target?: any;
		siblings?: any[];
		sortKey?: string;
		sortBefore?: boolean;
		updateData?: any;
	}): void;

	/* -------------------------------------------- */
	/*  Saving and Loading
	/* -------------------------------------------- */

	/**
	 * Clone an Entity, creating a new Entity using the current data as well as provided creation overrides.
	 *
	 * @param createData	Additional data which overrides current Entity data at the time of creation
	 * @param options		Additional creation options passed to the Entity.create method
	 * @returns				A Promise which resolves to the created clone Entity
	 */
	clone(createData?: object, options?: object): Promise<this>;

	/**
	 * Serializing an Entity should simply serialize it's inner data, not the entire instance
	 */
	toJSON(): Entity.Data<T>;

	/**
	 * Export entity data to a JSON file which can be saved by the client and later imported into a different session
	 */
	exportToJSON(): void;

	/**
	 * A helper function to handle obtaining the dropped Entity data from a dropped event. Entity drop data could have:
	 * 1. A compendium pack and entry id
	 * 2. A World Entity _id
	 * 3. A data object explicitly provided
	 *
	 * @param data	The data object extracted from a DataTransfer event
	 * @return		The Entity data that should be handled by the drop handler
	 */
	static fromDropData<T = Entity>(data: object): Promise<T>;

	/**
	 * Import data and update this entity
	 * @param json	JSON data string
	 * @return		The updated Entity
	 */
	importFromJSON(json: string): Promise<Entity>;

	/**
	 * Render an import dialog for updating the data related to this Entity through an exported JSON file
	 */
	importFromJSONDialog(): Promise<void>;

	/**
	 * Transform the Entity data to be stored in a Compendium pack.
	 * Remove any features of the data which are world-specific.
	 * This function is asynchronous in case any complex operations are required prior to exporting.
	 *
	 * @return	A data object of cleaned data ready for compendium import
	 */
	toCompendium(): Entity.Data<T>;

	/**
	 * Provide a Dialog form to create a new Entity of this type.
	 * Choose a name and a type from a select menu of types.
	 * @param data		Initial data with which to populate the creation form
	 * @param options	Initial positioning and sizing options for the dialog form
	 * @return			A Promise which resolves to the created Entity
	 */
	static createDialog(data?: object, options?: object): Promise<Entity>;
}
